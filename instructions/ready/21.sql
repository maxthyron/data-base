/*
21. Инструкция DELETE с вложенным коррелированным подзапросом в предложении WHERE.
-- Пример для базы данных AdventureWorks
DELETE FROM Production.Product
WHERE ProductID IN
(
SELECT Product.ProductID
FROM Production.Product LEFT OUTER JOIN Sales.SalesOrderDetail
ON Product.ProductID = SalesOrderDetail.ProductID
WHERE SalesOrderDetail.ProductID IS NULL
AND Product.ProductSubCategoryID = 5
)
*/

DELETE FROM Monsters
WHERE name IN
(
    SELECT name
    FROM Monsters
    WHERE type = 'Goblin' AND uniqueLoot = 'Kubinator'
)