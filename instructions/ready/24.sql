/*
24. Оконные функции. Использование конструкций MIN/MAX/AVG OVER()
-- Для каждой заданной группы продукта вывести среднее значение цены
SELECT P.ProductID,
P.UnitPrice,
P.ProductName,
OD.UnitPrice,
AVG(OD.UnitPrice) OVER(PARTITION BY P.productID, P.ProductName) AS AvgPrice,
MIN(OD.UnitPrice) OVER(PARTITION BY P.productID, P.ProductName) AS MinPrice,
MAX(OD.UnitPrice) OVER(PARTITION BY P.productID, P.ProductName) AS MaxPrice,
FROM Products P LEFT OUTER JOIN [Order Details] OD ON OD.ProductID = P.ProductID
*/

SELECT R.name, R.weather,
AVG(R.area) OVER(PARTITION BY R.name) AS AvgArea
FROM Regions R LEFT OUTER JOIN Keys K ON K.regionID = R.id