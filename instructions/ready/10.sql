/*
10. Инструкция SELECT, использующая поисковое выражение CASE.
SELECT ProductName,
CASE
WHEN UnitPrice < 10 THEN 'Inexpensive'
WHEN UnitPrice < 50 THEN 'Fair'
WHEN UNitPrice < 100 THEN 'Expensive'
ELSE 'Very Expensive'
END AS Price
FROM products
*/

-- Show age status of adventurers
SELECT name, surname, age,
CASE
    WHEN age < 10 THEN 'Child'
    WHEN age < 20 THEN 'Young'
    WHEN age < 50 THEN 'Middle age'
    ELSE 'Old'
END AS 'Age status'
FROM Adventurers
GO