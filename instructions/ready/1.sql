/*
1. Инструкция SELECT, использующая предикат сравнения

SELECT DISTINCT C1.City, C1.CompanyName
FROM Customers C1 JOIN Customers AS C2 ON C2.City = C1.City
WHERE C2.CustomerID <> C1.CustomerID AND C1.Country = 'Argentina'
ORDER BY C1.City, C1.CompanyName
*/

-- Collect party for the raid
SELECT name, surname, level, class
FROM Adventurers
WHERE level >= 60 AND (age BETWEEN 20 AND 40)
ORDER BY name
GO