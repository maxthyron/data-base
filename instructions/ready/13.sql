/*
13. Инструкция SELECT, использующая вложенные подзапросы с уровнем вложенности 3.
SELECT 'By units' AS Criteria, ProductName as 'Best Selling'
FROM ProductsWHERE ProductID =
(
    SELECT ProductID
    FROM [Order Details]
    GROUP BY ProductID
    HAVING SUM(Quantity) =
    (
        SELECT MAX(SQ)
        FROM
        (
            SELECT SUM(Quantity) as SQ
            FROM [Order Details]
            GROUP BY ProductID
        ) AS OD
    )
)
UNION
SELECT 'By revenue' AS Criteria, ProductName as 'Best Selling'
FROM Products
WHERE ProductID =
(
    SELECT ProductID
    FROM [Order Details]
    GROUP BY ProductID
    HAVING SUM(UnitPrice*Quantity*(1-Discount)) =
    (
        SELECT MAX(SQ)
        FROM
        (
            SELECT SUM(UnitPrice*Quantity*(1-Discount)) AS SQ
            FROM [Order Details]
            GROUP BY ProductID
        ) AS OD
    )
)
*/

SELECT *
FROM Adventurers
WHERE level = 
(
    SELECT level
    FROM Adventurers
    WHERE level < 45 AND character = 'Overjoyed' AND age =
    (
        SELECT age
        FROM Adventurers
        WHERE class = 'Dark Knight' AND id =
        (
            SELECT Adventurers.id
            FROM Adventurers
            JOIN Keys
            ON Keys.adventurerID = Adventurers.id
            WHERE date = '1887-04-06'
        )
    )
)