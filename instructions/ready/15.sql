/*
15. Инструкция SELECT, консолидирующая данные с помощью предложения GROUP BY и
предложения HAVING.
-- Получить список категорий продуктов, средняя цена которых больше общей средней
-- цены продуктов
SELECT CategoryID, AVG(UnitPrice) AS 'Average Price'
FROM Products P
GROUP BY CategoryID
HAVING AVG(UnitPrice) >
(
SELECT AVG(UnitPrice) AS MPrice
FROM Products
)
*/

SELECT type, AVG(level) AS 'Average Level'
FROM Monsters
GROUP BY type
HAVING AVG(level) > 50