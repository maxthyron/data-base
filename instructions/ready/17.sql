/*
17. Многострочная инструкция INSERT, выполняющая вставку в таблицу результирующего набора
данных вложенного подзапроса.
INSERT [Order Details] (OrderID, ProductID, Unitprice, Quantity, Discount)
SELECT (
    SELECT MAX(OrderID)
    FROM Orders
    WHERE CustomerID = 'ALFKI'
), ProductID, UnitPrice, 10, 0.1
FROM Products
WHERE ProductName = 'Tofu'
*/

INSERT Monsters (id, name, level, uniqueLoot, type)
SELECT DISTINCT
(
    SELECT MAX(id)
    FROM Monsters
) + 1, 'Kolya', 100, 'Kubinator', 'Goblin'
FROM Monsters
WHERE uniqueLoot = 'Kubinator'
