/*
4. Инструкция SELECT, использующая предикат IN с вложенным подзапросом.
-- Получить список заказов для клиентов из Лондона, оформленных через сотрудника 1
SELECT OrderID, CustomerID, EmployeeID, OrderDate
FROM Orders
WHERE CustomerID IN
(
SELECT CustomerID
FROM Customers
WHERE City = 'London'
) AND EmployeeID = 1
*/

-- All adventurers, who have slained vampires
SELECT name AS "Name", surname AS "Surname"
FROM Adventurers
JOIN Keys
ON Adventurers.id = Keys.adventurerID
WHERE Keys.monsterID IN
(
    SELECT id
    FROM Monsters
    WHERE type = 'Vampire'
)
GO