/*
7. Инструкция SELECT, использующая агрегатные функции в выражениях столбцов.
SELECT
AVG(TotalPrice) AS 'Actual AVG',
SUM(TotalPrice) / COUNT(OrderID) AS 'Calc AVG'
FROM (
SELECT OrderID, SUM(UnitPrice*Quantity*(1-Discount)) AS TotalPrice
FROM [Order Details]
GROUP BY OrderID
)
AS TotOrders
*/

--
SELECT AVG(age) AS 'Average Age'
FROM Adventurers
WHERE class = 'Priest'
GO