/*
25. Оконные фнкции для устранения дублей
Придумать запрос, в результае которого в данных появляются полные дубли. Устранить дублирующиеся
строки с использованием функции ROW_NUMBER()
*/

INSERT Keys (id, adventurerID, regionID, monsterID, date)
VALUES (101, 45, 45, 45, '2018-09-08')
INSERT Keys (id, adventurerID, regionID, monsterID, date)
VALUES (102, 45, 45, 45, '2018-09-08')
INSERT Keys (id, adventurerID, regionID, monsterID, date)
VALUES (103, 45, 45, 45, '2018-09-08')

WITH DOUBLES AS
(
    SELECT adventurerID, regionID, monsterID, date, ROW_NUMBER()
    OVER(PARTITION BY adventurerID, regionID, monsterID, date ORDER BY id) AS n
    FROM Keys
)
DELETE FROM DOUBLES
WHERE n > 1
