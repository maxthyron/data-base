/*
9. Инструкция SELECT, использующая простое выражение CASE.
SELECT CompanyName, OrderID,
CASE YEAR(OrderDate)
WHEN YEAR(Getdate()) THEN 'This Year'
WHEN YEAR(GetDate()) - 1 THEN 'Last year'
ELSE CAST(DATEDIFF(year, OrderDate, Getdate()) AS varchar(5)) + ' years ago'
END AS 'When'
FROM Orders JOIN Customers ON Orders.CustomerID = Customers.CustomerID
*/

-- Check adventures' progress 
SELECT name, surname, level, class,
CASE level
    WHEN 90 THEN 'Master'
    WHEN 1 THEN 'Beginner'
    WHEN 50 THEN 'Average'
    ELSE 'Making progress'
    END AS 'Training Status'
FROM Adventurers
GO