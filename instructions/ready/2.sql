/*
2. Инструкция SELECT, использующая предикат BETWEEN.

-- Получить список клиентов, сделавших заказы между '1997-01-01' и '1997-03-31'
SELECT DISTINCT CustomerID, OrderDate
FROM Orders
WHERE OrderDate BETWEEN '1997-01-01' AND '1997-03-31'
*/

-- Select starting region for beginner
SELECT name, difficulty
FROM Regions
WHERE difficulty BETWEEN 1 AND  3
ORDER BY difficulty
GO