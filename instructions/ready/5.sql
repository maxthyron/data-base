/*
5. Инструкция SELECT, использующая предикат EXISTS с вложенным подзапросом.
-- Получить список продуктов, которые никто никогда не заказывал
SELECT ProductID, ProductName
FROM Products
WHERE EXISTS
(
    SELECT Products.ProductID
    FROM Products
    LEFT OUTER JOIN [Order Details]
    ON Products.ProductID = [Order Details].ProductID
    WHERE [Order Details].ProductID IS NULL
)
*/

--
SELECT id
FROM Monsters AS M
WHERE EXISTS
(
    SELECT Monsters.id
    FROM Monsters
    LEFT OUTER JOIN Keys
    ON Keys.monsterID = Monsters.ids
    WHERE Keys.monsterID IS NULL AND Monsters.id = M.id
)
GO

