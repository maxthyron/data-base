/*
12. Инструкция SELECT, использующая вложенные коррелированные подзапросы в качестве
производных таблиц в предложении FROM.
SELECT 'By units' AS Criteria, ProductName as 'Best Selling'
FROM Products P JOIN
(
    SELECT TOP 1 ProductID, SUM(Quantity) AS SQ
    FROM [Order Details]
    GROUP BY productID
    ORDER BY SQ DESC
) AS OD ON OD.ProductID = P.ProductID
UNION
SELECT 'By revenue' AS Criteria, ProductName as 'Best Selling'
FROM Products P JOIN
(
    SELECT TOP 1 ProductID, SUM(UnitPrice*Quantity*(1-Discount)) AS SR
    FROM [Order Details]
    GROUP BY ProductID
    ORDER BY SR DESC
) AS OD ON OD.ProductID = P.ProductID
*/

SELECT name, class
FROM Adventurers A 
JOIN
(
    SELECT adventurerID, date
    FROM Keys
) AS AName ON AName.adventurerID = A.id
UNION
SELECT name, type
FROM Monsters M
JOIN
(
    SELECT monsterID, date
    FROM Keys
) AS MName ON MName.monsterID = M.id