/*
11. Создание новой временной локальной таблицы из результирующего набора данных инструкции
SELECT.
SELECT ProductID,
SUM(Quantity) AS SQ,
CAST(SUM(UnitPrice*Quantity*(1.0-Discount))AS money) AS SR
INTO #BestSelling
FROM [Order Details]
WHERE ProductID IS NOT NULL
GROUP BY productID
*/

SELECT *
INTO #Mages
FROM Adventurers
WHERE class = 'Mage' OR class = 'Mystic' OR class = 'Scholar'

SELECT * FROM #Mages
