/*
14. Инструкция SELECT, консолидирующая данные с помощью предложения GROUP BY , но без
предложения HAVING.
-- Для каждого заказанного продукта категории 1 получить его цену, среднюю цену,
-- минимальную цену и название продукта
SELECT P.ProductID,
P.UnitPrice,
AVG(OD.UnitPrice) AS AvgPrice,
MIN(OD.UnitPrice) AS MinPrice,
P.ProductName
FROM Products P LEFT OUTER JOIN [Order Details] OD ON OD.ProductID = P.ProductID
WHERE CategoryID = 1
GROUP BY P.productID, P.UnitPrice, P.ProductName
*/

SELECT name, difficulty, area
FROM Regions
GROUP BY name, difficulty, area