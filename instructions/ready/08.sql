/*
8. Инструкция SELECT, использующая скалярные подзапросы в выражениях столбцов.
SELECT ProductID, UnitPrice,
(
    SELECT AVG(UnitPrice)
    FROM [Order Details]
    WHERE [Order Details].ProductID = Products.ProductID
) AS AvgPrice,
(
    SELECT MIN(UnitPrice)FROM [Order Details]
    WHERE [Order Details].ProductID = Products.ProductID
) AS MaxPrice,
ProductName
FROM Products
WHERE CategoryID = 1
*/

SELECT difficulty, weather, area,
(
    SELECT SUM(area)
    FROM Regions
) AS AllArea,
name
FROM Regions
WHERE weather = 'Cloudy' and difficulty BETWEEN 6 AND 7