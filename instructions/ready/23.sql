/*
23. Инструкция SELECT, использующая рекурсивное обобщенное табличное выражение.
-- Создание таблицы.
CREATE TABLE dbo.MyEmployees
(
EmployeeID smallint NOT NULL,
FirstName nvarchar(30) NOT NULL,
LastName nvarchar(40) NOT NULL,
Title nvarchar(50) NOT NULL,
DeptID smallint NOT NULL,
ManagerID int NULL,
CONSTRAINT PK_EmployeeID PRIMARY KEY CLUSTERED (EmployeeID ASC)
) ;
GO
-- Заполнение таблицы значениями.
INSERT INTO dbo.MyEmployees VALUES (1, N'Иван', N'Петров', N'Главный исполнительный
директор',16,NULL) ;
...
GO
-- Определение ОТВ
WITH DirectReports (ManagerID, EmployeeID, Title, DeptID, Level)
AS
(
-- Определение закрепленного элемента
SELECT e.ManagerID, e.EmployeeID, e.Title, e.DeptID, 0 AS Level
FROM dbo.MyEmployees AS e
WHERE ManagerID IS NULL
UNION ALL
-- Определение рекурсивного элемента
SELECT e.ManagerID, e.EmployeeID, e.Title, e.DeptID, Level + 1
FROM dbo.MyEmployees AS e INNER JOIN DirectReports AS d ON e.ManagerID = d.EmployeeID
)
-- Инструкция, использующая ОТВ
SELECT ManagerID, EmployeeID, Title, DeptID, Level
FROM DirectReports ;
*/

WITH Elders(id, name, surname, age, character, level, class, iter)
AS
(
    SELECT a.id, a.name, a.surname, a.age, a.character, a.level, a.class, 0 AS iter
    FROM dbo.Adventurers as a
    WHERE age = 70
    UNION ALL
    SELECT a.id, a.name, a.surname, a.age, a.character, a.level, a.class, iter + 1
    FROM dbo.Adventurers as a INNER JOIN Elders AS e ON e.id = a.id + 1
)
SELECT *
FROM Elders