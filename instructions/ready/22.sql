/*
22. Инструкция SELECT, использующая простое обобщенное табличное выражение
-- Пример для базы данных SPJ
WITH CTE (SupplierNo, NumberOfShips)
AS
(
SELECT Sno, COUNT(*) AS Total
FROM SPJ
WHERE Sno IS NOT NULL
GROUP BY Sno
)
SELECT AVG(NumberOfShips) AS 'Среднее количество поставок для поставщиков'
FROM CTE
*/

WITH Mages (name, level, character, age)
AS
(
    SELECT name, level, character, age
    FROM Adventurers
    WHERE class = 'Mage'
)
SELECT AVG(age)
FROM Mages