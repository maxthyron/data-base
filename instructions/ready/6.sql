/*
6. Инструкция SELECT, использующая предикат сравнения с квантором.
-- Получить список продуктов, цена которых больше цены любого продукта категории 2
SELECT ProductID, ProductName, UnitPrice
FROM Products
WHERE UnitPrice > ALL
(
SELECT UnitPrice
FROM Products
WHERE CategoryID = 2
)
*/

-- The oldest Priest
SELECT *
FROM Adventurers
WHERE class = 'Priest' AND age >= ALL
(
    SELECT age
    FROM Adventurers
    WHERE class = 'Priest'
)
GO

SELECT *
FROM Adventurers
WHERE class = 'Priest'
GO