USE ProjectDB;
GO

CREATE TABLE Table1
(
    id INT NOT NULL,
    var1 NVARCHAR(1) NOT NULL,
    valid_from_dttm DATE NOT NULL,
    valid_to_dttm DATE NOT NULL
    
) ;
GO

CREATE TABLE Table2
(
    id INT NOT NULL,
    var2 NVARCHAR(1) NOT NULL,
    valid_from_dttm DATE NOT NULL,
    valid_to_dttm DATE NOT NULL
) ;
GO

INSERT Table1 (id, var1, valid_from_dttm, valid_to_dttm)
VALUES (1, 'A', '2018-09-01', '2018-09-15')
INSERT Table1 (id, var1, valid_from_dttm, valid_to_dttm)
VALUES (1, 'B', '2018-09-16', '5999-12-31')

INSERT Table2 (id, var2, valid_from_dttm, valid_to_dttm)
VALUES (1, 'A', '2018-09-01', '2018-09-18')
INSERT Table2 (id, var2, valid_from_dttm, valid_to_dttm)
VALUES (1, 'B', '2018-09-19', '5999-12-31')

INSERT Table1 (id, var1, valid_from_dttm, valid_to_dttm)
VALUES (2, 'A', '2018-09-01', '2018-09-15')
INSERT Table1 (id, var1, valid_from_dttm, valid_to_dttm)
VALUES (2, 'B', '2018-09-16', '5999-12-31')

INSERT Table2 (id, var2, valid_from_dttm, valid_to_dttm)
VALUES (2, 'A', '2018-09-01', '2018-09-18')
INSERT Table2 (id, var2, valid_from_dttm, valid_to_dttm)
VALUES (2, 'B', '2018-09-19', '5999-12-31')

SELECT * FROM Table1
SELECT * FROM Table2


SELECT id, var1, valid_from_dttm INTO #NT_1 FROM Table1
UNION
SELECT id, var2, valid_from_dttm FROM Table2
ORDER BY valid_from_dttm

SELECT id, var1, valid_to_dttm INTO #NT_2 FROM Table1
UNION
SELECT id, var2, valid_to_dttm FROM Table2
ORDER BY valid_to_dttm

SELECT * FROM #NT_1
SELECT * FROM #NT_2

SELECT ROW_NUMBER() OVER(ORDER BY id) AS ROWN, id, var1, valid_from_dttm INTO #NTT_1 FROM #NT_1
SELECT ROW_NUMBER() OVER(ORDER BY id) AS ROWN, id, var1, valid_to_dttm INTO #NTT_2 FROM #NT_2

SELECT T1.id, T1.var1, T2.var1, T1.valid_from_dttm, T2.valid_to_dttm
FROM dbo.#NTT_1 T1 JOIN dbo.#NTT_2 T2 ON T1.ROWN = T2.ROWN
ORDER BY id, T1.var1, T2.var1