/*
3. Инструкция SELECT, использующая предикат LIKE.
-- Получить список категорий продуктов в описании которых присутствует слово 'pasta'
SELECT DISTINCT CategoryName
FROM Categories JOIN Products ON Products.categoryID = Categories.CategoryID
WHERE Description LIKE '%pasta%'
*/

-- Show all monsters, that have Daggers in their inventory
SELECT name, type, uniqueLoot
FROM Monsters
WHERE uniqueLoot LIKE '%dagger%'
ORDER BY name
GO