/*
19. Инструкция UPDATE со скалярным подзапросом в предложении SET.
UPDATE Products
SET UnitPrice =
(
SELECT AVG(UnitPrice)
FROM [Order Details]
WHERE ProductID = 37
)
WHERE ProductID = 37
*/

UPDATE Monsters
SET level =
(
    SELECT AVG(level)
    FROM Monsters
    WHERE type = 'Vampire'
)
WHERE (uniqueLoot = 'Flesh') AND (type = 'Vampire')