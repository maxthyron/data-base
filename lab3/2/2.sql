USE ProjectDB
GO

IF OBJECT_ID ( N'dbo.SelectElders', 'P' ) IS NOT NULL
      DROP PROCEDURE dbo.SelectElders
GO

CREATE PROCEDURE dbo.SelectElders
AS
    WITH Elders(id, name, surname, age, character, level, class)
    AS
    (
        SELECT a.id, a.name, a.surname, a.age, a.character, a.level, a.class
        FROM dbo.Adventurers as a
        WHERE age = 70
        UNION ALL
        SELECT A.id, A.name, A.surname, A.age, A.character, A.level, A.class
        FROM dbo.Adventurers as A INNER JOIN Elders AS E ON E.id = A.id + 1
        WHERE A.age >= 40
    )
    SELECT *
    FROM Elders
GO

EXEC dbo.SelectElders