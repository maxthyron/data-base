USE ProjectDB
GO

IF OBJECT_ID ( N'dbo.SelectWizard', 'P' ) IS NOT NULL
      DROP PROCEDURE dbo.SelectWizard
GO

CREATE PROCEDURE dbo.SelectWizard
AS
      SELECT *
      FROM Adventurers
      WHERE class = 'Wizard'
GO

EXEC dbo.SelectWizard
GO