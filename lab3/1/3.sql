USE ProjectDB
GO
IF OBJECT_ID (N'dbo.DarkestRegions', N'FN') IS NOT NULL
    DROP FUNCTION dbo.DarkestRegions
GO

CREATE FUNCTION dbo.DarkestRegions()
RETURNS @HighLevelRegions TABLE
(
    Name NVARCHAR(30) NOT NULL, 
    Difficulty INT NOT NULL, 
    Weather NVARCHAR(15) NOT NULL,
    Area INT NOT NULL
)
AS
BEGIN
    INSERT INTO @HighLevelRegions
    SELECT R.name, R.difficulty, R.weather, R.area
    FROM Regions R
    WHERE R.difficulty > 8
RETURN
END
GO


SELECT *
FROM dbo.DarkestRegions()