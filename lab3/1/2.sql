USE ProjectDB
GO
IF OBJECT_ID (N'dbo.Healers', N'FN') IS NOT NULL
    DROP FUNCTION dbo.Healers
GO

CREATE FUNCTION dbo.Healers()
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN (
    SELECT name, surname, class, age
    FROM dbo.Adventurers
    WHERE class = 'Healer' OR class = 'Priest'
    )
GO


SELECT *
FROM Healers()
GO