IF OBJECT_ID ( N'dbo.CheckRegionUpdate', 'TR' ) IS NOT NULL
    PRINT 'YES'
    DROP TRIGGER dbo.CheckRegionUpdate
GO

CREATE TRIGGER CheckRegionUpdate
ON Regions
AFTER UPDATE
AS
BEGIN
    SELECT * FROM inserted
END;