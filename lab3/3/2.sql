CREATE TRIGGER DenyInsert 
ON Keys
INSTEAD OF INSERT
AS
BEGIN
    RAISERROR('This is uneditable table.',10,1);
END;

INSERT Keys VALUES (104, 5, 5, 5, '2018-09-08')