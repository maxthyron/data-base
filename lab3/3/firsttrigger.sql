IF OBJECT_ID ( N'dbo.NewTrigger', 'TR' ) IS NOT NULL
    PRINT 'YES'
    DROP TRIGGER ProjectDB.NewTTrigger
GO

DROP TRIGGER Newtriggerr ON DATABASE
GO
SELECT OBJECT_NAME(parent_id) as Table_Name, * FROM [ProjectDB].sys.triggers
GO


CREATE TRIGGER NewTTTrigger  
ON DATABASE   
FOR CREATE_TABLE
AS
BEGIN
    SELECT TOP 1 O.name, O.create_date, C.name
    FROM sys.all_objects O
    JOIN sys.all_columns C ON O.object_id=C.object_id
    WHERE type_desc = 'USER_TABLE'
    ORDER BY O.create_date DESC
END;

SELECT * FROM sys.all_objects
WHERE type_desc = 'USER_TABLE'

SELECT O.name, O.create_date, C.name
FROM sys.all_objects O
JOIN sys.all_columns C ON O.object_id=C.object_id
WHERE type_desc = 'USER_TABLE'

SELECT OBJECT_NAME(parent_id) as Table_Name, * FROM [ProjectDB].sys.triggers