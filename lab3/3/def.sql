DROP TRIGGER IF EXISTS NewTriggerCreate ON DATABASE
GO
CREATE TRIGGER NewTriggerCreate
ON DATABASE   
FOR CREATE_TABLE
AS
BEGIN
    DECLARE @Schema SYSNAME = eventdata().value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname');
    DECLARE @Table SYSNAME = eventdata().value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname');

    PRINT 'Created table: [' + @Schema + '].[' + @Table + ']';
/*    SELECT TOP 1 O.name, O.create_date, C.name
    FROM sys.all_objects O
    JOIN sys.all_columns C ON O.object_id=C.object_id
    WHERE type_desc = 'USER_TABLE'
    ORDER BY O.create_date DESC*/
END;
GO

DROP TRIGGER IF EXISTS NewTriggerDrop ON DATABASE
GO
CREATE TRIGGER NewTriggerDrop
ON DATABASE   
FOR DROP_TABLE
AS
BEGIN
    DECLARE @Schema SYSNAME = eventdata().value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname');
    DECLARE @Table SYSNAME = eventdata().value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname');

    PRINT 'Table dropped: [' + @Schema + '].[' + @Table + ']';
END;
GO

DROP TRIGGER IF EXISTS NewTriggerAlter ON DATABASE
GO
CREATE TRIGGER NewTriggerAlter
ON DATABASE   
FOR ALTER_TABLE
AS
BEGIN
    DECLARE @Schema SYSNAME = eventdata().value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname');
    DECLARE @Table SYSNAME = eventdata().value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname');

    PRINT 'Altered table: [' + @Schema + '].[' + @Table + ']';
END;
GO


CREATE TABLE TEMP (id INT, string VARCHAR(30))
GO
ALTER TABLE TEMP ALTER COLUMN string NVARCHAR(20) NOT NULL; 
GO
DROP TABLE TEMP
GO
