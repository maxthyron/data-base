-- Clear tables

USE ProjectDB;
GO

DELETE Keys;
DELETE Adventurers;
DELETE Monsters;
DELETE Regions;
GO