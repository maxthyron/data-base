-- Drop tables

USE ProjectDB;
GO

DROP TABLE Keys;
DROP TABLE Adventurers;
DROP TABLE Monsters;
DROP TABLE Regions;
GO