-- Constraints on tables

USE ProjectDB;
GO

ALTER TABLE ProjectDB.dbo.Adventurers ADD
CONSTRAINT AgeCheck CHECK (age BETWEEN 18 and 70),
CONSTRAINT LevelCheck CHECK (level BETWEEN 1 and 100)
GO

ALTER TABLE ProjectDB.dbo.Regions ADD
CONSTRAINT DifficultyCheck CHECK (difficulty BETWEEN 0 and 10),
CONSTRAINT AreaCheck CHECK (area >= 10000)
GO

ALTER TABLE ProjectDB.dbo.Monsters ADD
CONSTRAINT MonsterLevelCheck CHECK (level BETWEEN 1 and 100)
GO