-- Create tables

CREATE DATABASE ProjectDB;
GO

USE ProjectDB;
GO

CREATE TABLE Adventurers (
    id INT NOT NULL PRIMARY KEY,
    name NVARCHAR(30) NOT NULL,
    surname NVARCHAR(30) NOT NULL,
    age INT NOT NULL,
    character NVARCHAR(15) NOT NULL,
    level INT NOT NULL,
    class NVARCHAR(15) NOT NULL
);
GO

CREATE TABLE Regions (
    id INT NOT NULL PRIMARY KEY,
    name NVARCHAR(30) NOT NULL,
    difficulty INT NOT NULL,
    weather NVARCHAR(15) NOT NULL,
    area INT NOT NULL
);
GO

CREATE TABLE Monsters (
    id INT NOT NULL PRIMARY KEY,
    name NVARCHAR(30) NOT NULL,
    level INT NOT NULL,
    uniqueLoot NVARCHAR(30) NOT NULL,
    type NVARCHAR(30) NOT NULL
);
GO

CREATE TABLE Keys (
    id INT NOT NULL PRIMARY KEY,
    adventurerID INT NOT NULL FOREIGN KEY REFERENCES Adventurers(id),
    regionID INT NOT NULL FOREIGN KEY REFERENCES Regions(id),
    monsterID INT NOT NULL FOREIGN KEY REFERENCES Monsters(id),
    date DATE NOT NULL
);
GO