-- Import data

USE ProjectDB;
GO

BULK INSERT ProjectDB.dbo.Adventurers 
FROM "/DBFiles/csv/adventurers.csv"
WITH (DATAFILETYPE = 'char', FIRSTROW = 2, FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a');
GO

BULK INSERT ProjectDB.dbo.Monsters 
FROM "/DBFiles/csv/monsters.csv"
WITH (DATAFILETYPE = 'char', FIRSTROW = 2, FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a');
GO

BULK INSERT ProjectDB.dbo.Regions
FROM "/DBFiles/csv/regions.csv"
WITH (DATAFILETYPE = 'char', FIRSTROW = 2, FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a');
GO

BULK INSERT ProjectDB.dbo.Keys 
FROM "/DBFiles/csv/keys.csv"
WITH (DATAFILETYPE = 'char', FIRSTROW = 2, FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a');
GO
