﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Lab8.NET
{
    class Tasks
    {
        private readonly string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";

        static void printMenu()
        {
            Console.WriteLine("1. Connection String");
            Console.WriteLine("2. Scalar Selection");
            Console.WriteLine("3. SQL Data Reader");
            Console.WriteLine("4. Command With Parameters");
            Console.WriteLine("5. Stored Procedure");
            Console.WriteLine("6. DataSet From Table");
            Console.WriteLine("7. Filter Sort");
            Console.WriteLine("8. Insert");
            Console.WriteLine("9. Delete");
            Console.WriteLine("10. XML");
            Console.WriteLine("\n -- 0. Exit");
        }
        static void Main(string[] args)
        {
            Tasks app = new Tasks();
            bool flag = true;

            while (flag)
            {
                printMenu();
                Console.Write("\nEnter your choice > ");
                string choice = Console.ReadLine();
                Console.Clear();
                switch (choice)
                {
                    case "0":
                        flag = false;
                        break;
                    case "1":
                        app.connectedObjects_task_1_ConnectionString();
                        break;
                    case "2":
                        app.connectedObjects_task_2_SimpleScalarSelection();
                        break;
                    case "3":
                        app.connectedObjects_task_3_SqlCommand_SqlDataReader();
                        break;
                    case "4":
                        app.connectedObjects_task_4_SqlCommandWithParameters();
                        break;
                    case "5":
                        app.connectedObjects_task_5_SqlCommand_StoredProcedure();
                        break;
                    case "6":
                        app.disconnectedObjects_task_6_DataSetFromTable();
                        break;
                    case "7":
                        app.disconnectedObjects_task_7_FilterSort();
                        break;
                    case "8":
                        app.disconnectedObjects_8_Insert();
                        break;
                    case "9":
                        app.disconnectedObjects_9_Delete();
                        break;
                    case "10":
                        app.disconnectedObjects_10_Xml();
                        break;
                }
                Console.Write("\nEnter any key...");
                Console.Read();
                Console.Clear();
            }
        }

        public void connectedObjects_task_1_ConnectionString()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Console.WriteLine("Connection properties:");
                Console.WriteLine("\tConnection string: {0}", connection.ConnectionString);
                Console.WriteLine("\tDatabase:          {0}", connection.Database);
                Console.WriteLine("\tData Source:       {0}", connection.DataSource);
                Console.WriteLine("\tServer version:    {0}", connection.ServerVersion);
                Console.WriteLine("\tConnection state:  {0}", connection.State);
                Console.WriteLine("\tWorkstation id:    {0}", connection.WorkstationId);
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the connection creating. Message: " + e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void connectedObjects_task_2_SimpleScalarSelection()
        {
            string queryString = @"SELECT COUNT(*) FROM Adventurers";
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand scalarQueryCommand = new SqlCommand(queryString, connection);
            Console.WriteLine("Sql command \"{0}\" has been created.", queryString);
            try
            {
                connection.Open();
                Console.WriteLine("-------->>> The count of Adventurers is {0}", scalarQueryCommand.ExecuteScalar());
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql command execution. Message: " + e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void connectedObjects_task_3_SqlCommand_SqlDataReader()
        {
            string queryString = @"SELECT name, surname, class FROM Adventurers WHERE name LIKE 'K%'";
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand dataQueryCommand = new SqlCommand(queryString, connection);
            Console.WriteLine("Sql command \"{0}\" has been created.", queryString);
            try
            {
                connection.Open();
                SqlDataReader dataReader = dataQueryCommand.ExecuteReader();

                Console.WriteLine("-------->>> Adventurers with name starting with K: ");
                while (dataReader.Read())
                {
                    Console.WriteLine("\t{0} {1}", dataReader.GetValue(0), dataReader.GetValue(1));
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql command execution. Message: " + e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void connectedObjects_task_4_SqlCommandWithParameters()
        {
            string countQueryString = @"SELECT COUNT(*) FROM Adventurers";
            string insertQueryString = @"INSERT INTO Adventurers(id, name, surname, age, character, level, class) VALUES (";
            string deleteQueryString = @"DELETE FROM Adventurers WHERE name = @name";

            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand countQueryCommand = new SqlCommand(countQueryString, connection);
            SqlCommand deleteQueryCommand = new SqlCommand(deleteQueryString, connection);

            deleteQueryCommand.Parameters.Add("@name", SqlDbType.VarChar, 85);

            Console.WriteLine("Sql commands: \n1) \"{0}\"\n2) \"{1}\"\n3) \"{2}\"\nhas been created.\n", countQueryString, insertQueryString, deleteQueryString);
            try
            {
                connection.Open();

                Console.WriteLine("Current count of adventurers: {0}", countQueryCommand.ExecuteScalar());
                Console.WriteLine("Inserting a new adventurer. Input: ");
                Console.Write("- name = ");
                string name = Console.ReadLine();
                Console.Write("- surname = ");
                string surname = Console.ReadLine();
                Console.Write("- age = ");
                int age = Convert.ToInt32(Console.ReadLine());
                Console.Write("- character = ");
                string character = Console.ReadLine();
                Console.Write("- level = ");
                int level = Convert.ToInt32(Console.ReadLine());
                Console.Write("- class = ");
                string aclass = Console.ReadLine();


                insertQueryString += ((Int32)countQueryCommand.ExecuteScalar() + 1).ToString() + "," + "'" + name + "'" + "," + "'" + surname + "'" + "," + age + "," + "'" + character + "'" + "," + level + "," + "'" + aclass + "'" + ")";
                SqlCommand insertQueryCommand = new SqlCommand(insertQueryString, connection);

                deleteQueryCommand.Parameters["@name"].Value = name;

                Console.WriteLine("\nInsert command: {0}", insertQueryCommand.CommandText);
                var insCount = insertQueryCommand.ExecuteNonQuery();
                Console.WriteLine("------>>> New count of Adventurers: {0} (rows affected: {1})", countQueryCommand.ExecuteScalar(), insCount);

                Console.WriteLine("Delete command: {0}", deleteQueryCommand.CommandText);
                var delCount = deleteQueryCommand.ExecuteNonQuery();
                Console.WriteLine("------>>> New count of Adventurers: {0} (rows affected: {1})", countQueryCommand.ExecuteScalar(), delCount);
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql command execution. Message: " + e.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Bad input! " + ex.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void connectedObjects_task_5_SqlCommand_StoredProcedure()
        {
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand storedProcedureCommand = connection.CreateCommand();
            storedProcedureCommand.CommandType = CommandType.StoredProcedure;
            storedProcedureCommand.CommandText = "CalculateFactorial1";

            Console.WriteLine("Sql command \"{0}\" has been created.", storedProcedureCommand.CommandText);
            try
            {
                connection.Open();

                Console.Write("Factorial. Input the number: ");
                int number = Convert.ToInt32(Console.ReadLine());
                storedProcedureCommand.Parameters.Add("n", SqlDbType.Int).Value = number;

                var returnParameter = storedProcedureCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                storedProcedureCommand.ExecuteNonQuery();
                var result = returnParameter.Value;

                Console.WriteLine("------>>> {0}! = {1} ", number, result);
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql command execution. Message: " + e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void disconnectedObjects_task_6_DataSetFromTable()
        {
            string query = @"SELECT name, surname, id FROM Adventurers WHERE class = 'Wizard'";

            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Wizards");
                DataTable table = dataSet.Tables["Wizards"];

                Console.WriteLine("Adventurers who are Wizards:");
                foreach (DataRow row in table.Rows)
                {
                    Console.Write("{0} ", row["name"]);
                    Console.Write(" ---- {0}\n", row["id"]);
                }
                Console.WriteLine();
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql query execution. Message: " + e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void disconnectedObjects_task_7_FilterSort()
        {
            string query = @"SELECT * FROM Adventurers";
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "AdventurersXML");
                DataTableCollection tables = dataSet.Tables;

                Console.Write("Input part of name of Adventurer: ");
                string partOfName = Console.ReadLine();
                Console.WriteLine();

                string filter = "name like '%" + partOfName + "%'";
                string sort = "name asc";
                Console.WriteLine("Adventurer who have name like \"" + partOfName + "\":");
                foreach (DataRow row in tables["AdventurersXML"].Select(filter, sort))
                {
                    Console.Write("{0} ", row["id"]);
                    Console.Write("{0} ", row["name"]);
                    Console.Write("{0} ", row["surname"]);
                    Console.Write("{0}\n", row["class"]);
                }
                Console.WriteLine();
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql query execution. Message: " + e.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Bad input! Message: " + ex.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void disconnectedObjects_8_Insert()
        {
            string dataCommand = @"SELECT * FROM Adventurers";
            string insertQueryString = @"INSERT INTO Adventurers(id, name, surname, age, character, level, class) VALUES (@id, @name, @surname, @age, @character, @level, @class)";

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();

                Console.WriteLine("Inserting a new adventurer. Input: ");
                Console.Write("- name = ");
                string name = Console.ReadLine();
                Console.Write("- surname = ");
                string surname = Console.ReadLine();
                Console.Write("- age = ");
                int age = Convert.ToInt32(Console.ReadLine());
                Console.Write("- character = ");
                string character = Console.ReadLine();
                Console.Write("- level = ");
                int level = Convert.ToInt32(Console.ReadLine());
                Console.Write("- class = ");
                string aclass = Console.ReadLine();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(new SqlCommand(dataCommand, connection));
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Adventurers");
                DataTable table = dataSet.Tables["Adventurers"];

                int id = table.Rows.Count;

                DataRow insertingRow = table.NewRow();
                insertingRow["id"] = id + 1;
                insertingRow["name"] = name;
                insertingRow["surname"] = surname;
                insertingRow["age"] = age;
                insertingRow["character"] = character;
                insertingRow["level"] = level;
                insertingRow["class"] = aclass;

                table.Rows.Add(insertingRow);

                Console.WriteLine("Adventurers");
                foreach (DataRow row in table.Rows)
                {
                    Console.Write("{0} ", row["name"]);
                    Console.Write("{0} ", row["surname"]);
                    Console.Write("---- {0}\n", row["class"]);
                }

                SqlCommand insertQueryCommand = new SqlCommand(insertQueryString, connection);
                insertQueryCommand.Parameters.Add("@id", SqlDbType.Int, 4, "id");
                insertQueryCommand.Parameters.Add("@name", SqlDbType.VarChar, 30, "name");
                insertQueryCommand.Parameters.Add("@surname", SqlDbType.VarChar, 30, "surname");
                insertQueryCommand.Parameters.Add("@age", SqlDbType.Int, 11, "age");
                insertQueryCommand.Parameters.Add("@character", SqlDbType.VarChar, 30, "character");
                insertQueryCommand.Parameters.Add("@level", SqlDbType.Int, 11, "level");
                insertQueryCommand.Parameters.Add("@class", SqlDbType.VarChar, 30, "class");

                dataAdapter.InsertCommand = insertQueryCommand;
                dataAdapter.Update(dataSet, "Adventurers");
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql command execution. Message: " + e.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Bad input! " + ex.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void disconnectedObjects_9_Delete()
        {
            string dataCommand = @"SELECT * FROM Adventurers";
            string deleteQueryString = @"DELETE FROM Adventurers WHERE name = @name";

            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();

                Console.WriteLine("Deleting the adventurer. Input: ");
                Console.Write("- name = ");
                string name = Console.ReadLine();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(new SqlCommand(dataCommand, connection));
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Adventurers");
                DataTable table = dataSet.Tables["Adventurers"];

                string filter = "name = '" + name + "'";
                foreach (DataRow row in table.Select(filter))
                {
                    row.Delete();
                }

                SqlCommand deleteQueryCommand = new SqlCommand(deleteQueryString, connection);
                deleteQueryCommand.Parameters.Add("@name", SqlDbType.VarChar, 85, "name");

                dataAdapter.DeleteCommand = deleteQueryCommand;
                dataAdapter.Update(dataSet, "Adventurers");

                Console.WriteLine("Adventurers");
                foreach (DataRow row in table.Rows)
                {
                    Console.Write("{0} ", row["name"]);
                    Console.Write("{0} ", row["surname"]);
                    Console.Write("---- {0}\n", row["class"]);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql command execution. Message: " + e.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Bad input! " + ex.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }

        public void disconnectedObjects_10_Xml()
        {
            string query = @"SELECT * FROM Adventurers";

            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Adventurer");
                DataTable table = dataSet.Tables["Adventurer"];

                dataSet.WriteXml("New.xml");
                Console.WriteLine("Check the New.xml file.");
            }
            catch (SqlException e)
            {
                Console.WriteLine("There is a problem during the sql query execution. Message: " + e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Connection has been closed.");
            }
        }
    }
}
