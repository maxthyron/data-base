using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class SqlServerStoresProcClass{

    [Microsoft.SqlServer.Server.SqlProcedure]
        public static void GetConcatenatedNames(string role)
        {
            using (SqlConnection contextConnection = new SqlConnection("context connection = true"))
            {
                SqlCommand contextCommand =
                   new SqlCommand(
                   "Select dbo.Concatenator(p_first_name) from passengers " +
                   "where p_last_name = @Role Group By p_first_name", contextConnection);

                contextCommand.Parameters.AddWithValue("@Role", role);
                contextConnection.Open();

                SqlContext.Pipe.ExecuteAndSend(contextCommand);
            }
        }

    [Microsoft.SqlServer.Server.SqlProcedure]
        public static void GetNames(string role)
        {
            using (SqlConnection contextConnection = new SqlConnection("context connection = true"))
            {
                SqlCommand contextCommand =
                   new SqlCommand(
                   "Select p_first_name from passengers " +
                   "where p_last_name = @Role", contextConnection);

                contextCommand.Parameters.AddWithValue("@Role", role);

                contextConnection.Open();

                // first, create the record and specify the metadata for the results
                SqlDataRecord rec = new SqlDataRecord(
                    new SqlMetaData("PersonName", SqlDbType.NVarChar, 200)
                    );

                // start a new result-set
                SqlContext.Pipe.SendResultsStart(rec);

                // send rows
                SqlDataReader rdr = contextCommand.ExecuteReader();
                while (rdr.Read())
                {
                    rec.SetString(0, rdr.GetString(0));
                    SqlContext.Pipe.SendResultsRow(rec);
                }

                // complete the result-set
                SqlContext.Pipe.SendResultsEnd();
            }
        }
}

