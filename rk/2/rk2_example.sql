-- CREATE DATABASE RK_2

DROP TABLE STUF, MED, DEPARTMENT, STUFF_MED

CREATE TABLE FLORIST(
    FLOR_ID int not null IDENTITY(1, 1) primary key,
    FLOR_NAME VARCHAR(30) not null,
    PAS VARCHAR(30) not null,
    PHONE VARCHAR(30) not null
)

CREATE TABLE BUKET(
    BUK_ID int not null IDENTITY(1, 1) primary key,
    AUTHOR int not null FOREIGN KEY REFERENCES FLORIST(FLOR_ID),
    BUK_NAME VARCHAR(20) not null
)

CREATE TABLE POKUPATEL(
    POK_ID int not null IDENTITY(1, 1) primary key,
    POK_NAME VARCHAR(30) not null,
    DOB DATE not null,
    CITY VARCHAR(30) not null,
    PHONE VARCHAR(30) not null
)

CREATE TABLE FLOR_BUKET(
    FLOR_ID int not null FOREIGN KEY REFERENCES FLORIST(FLOR_ID),
    BUK_ID int not null FOREIGN KEY REFERENCES BUKET(BUK_ID)
)


INSERT INTO FLORIST (FLOR_NAME, PAS, PHONE) VALUES
('Ellis Rodney', 105433270, 720249862),
('Walsh George',69878056, 122666436),
('Ray Geoffrey',868593312, 204898209),
('Murphy Ethan',4903653, 340199005),
('Singleton Ethan',175919675, 616058210),
('Carroll Clyde',804502010, 18864872),
('Cain Homer',4155174, 132346190),
('Webb Gordon',767009151, 586111982),
('Little Bryce',786320518, 765729016),
('Riley Joseph',3333333, 74189803)
;

INSERT INTO POKUPATEL (POK_NAME, DOB, CITY, PHONE) VALUES
('Carroll Clyde','2016-11-20', 'Jersey', 18864872),
('Cain Homer','2018-11-28', 'Moscow', 132346190),
('Webb Gordon','2018-11-30', 'Georgia', 586111982),
('Little Bryce','2018-12-04', 'Paris', 765729016),
('Riley Joseph','2018-12-05', 'Rome', 74189803),
('Ellis Rodney','2018-12-18', 'Voronezh', 720249862),
('Walsh George','2018-12-19', 'Voronezh', 122666436),
('Ray Geoffrey','2018-12-24', 'Voronezh', 204898209),
('Murphy Ethan','2018-12-26', 'Voronezh', 340199005),
('Singleton Ethan','2018-12-31', 'Voronezh', 616058210)
;

INSERT INTO BUKET (BUK_NAME, AUTHOR) VALUES
('Lurence',23),
('Izabelle',25),
('Rock',24),
('Rose',29),
('Peaks',22),
('Adam',21),
('Texas', 28),
('Fox',26),
('Grey monday',27),
('Hot',30)
;

INSERT INTO FLOR_BUKET (BUK_ID, FLOR_ID) VALUES
(3 ,3),
(4 ,5),
(4 ,4),
(5 ,9),
(7 ,2),
(4 ,1),
(4 ,8),
(7 ,6),
(5 ,7),
(9 ,0)
;

DROP TABLE FLORIST
SELECT * FROM POKUPATEL


SELECT BUKET.AUTHOR, BUK_NAME,
CASE BUKET.AUTHOR
    WHEN 26
    THEN 'VORONEZH!!!'
    ELSE 'NE VORONEZH!!!'
END comment
FROM BUKET
WHERE AUTHOR > 23



UPDATE POKUPATEL
SET DOB = 
    (
        SELECT MIN(DOB)
        FROM POKUPATEL
    )
WHERE POK_ID = 
    (
        SELECT TOP 1 POK_ID
        FROM POKUPATEL
        WHERE CITY = 'Voronezh'
    )



SELECT POK_NAME, DOB FROM POKUPATEL
GROUP BY POK_NAME, DOB
HAVING DOB > '2016-11-20'




USE master
GO
CREATE PROCEDURE check_info (@data_base CHAR(10))
    BEGIN

    END


SELECT * FROM sys.all_parameters