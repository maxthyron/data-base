﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Linq.Mapping;
using System.Xml.Linq;
using System.Data.Linq;
using System.Reflection;

namespace RK3
{
    class Program
    {
        static string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";

        [Table(Name = "Teachers")]
        class Teachers
        {
            [Column(Name = "TID", IsPrimaryKey = true)]
            public int TID { get; set; }

            [Column(Name = "TFIO")]
            public string TFIO { get; set; }

            [Column(Name = "TKAF")]
            public string TKAF { get; set; }

            [Column(Name = "TMAXS")]
            public int TMAXS { get; set; }
        }

        [Table(Name = "Students")]
        class Students
        {
            [Column(Name = "STID", IsPrimaryKey = true)]
            public int STID { get; set; }

            [Column(Name = "SFIO")]
            public string SFIO { get; set; }

            [Column(Name = "BDAY")]
            public DateTime BDAY { get; set; }

            [Column(Name = "SKAF")]
            public string SKAF { get; set; }

            [Column(Name = "TID", DbType = "INT", CanBeNull = true)]
            public int? TID { get; set; }
        }

        static void printMenu()
        {
            Console.WriteLine("1. LINQ - 1");
            Console.WriteLine("2. LINQ - 2");
            Console.WriteLine("3. LINQ - 3");
            Console.WriteLine("4. ADO - 1");
            Console.WriteLine("5. ADO - 2");
            Console.WriteLine("6. ADO - 3");
            Console.WriteLine("7. CLR");
            Console.WriteLine("\n -- 0. Exit");
        }

        static void Main()
        {
            bool flag = true;

            while (flag)
            {
                printMenu();
                Console.Write("\nEnter your choice > ");
                string choice = Console.ReadLine();
                Console.Clear();
                switch (choice)
                {
                    case "0":
                        flag = false;
                        break;
                    case "1":
                        firstLINQ();
                        break;
                    case "2":
                        secondLINQ();
                        break;
                    case "3":
                        thirdLINQ();
                        break;
                    case "4":
                        firstADO();
                        break;
                    case "5":
                        secondADO();
                        break;
                    case "6":
                        thirdADO();
                        break;

                }
                Console.Write("\nEnter any key...");
                Console.Read();
                Console.Clear();
            }

        }

        static void firstLINQ()
        {
            DataContext db = new DataContext(connectionString);

            var query = from s in db.GetTable<Students>()
                        where s.TID == null
                        orderby s.SKAF
                        select s;

            List<string> kafs = new List<string>();

            foreach (var s in query)
            {
                if (!kafs.Contains(s.SKAF))
                {
                    kafs.Add(s.SKAF);
                }
            }

            int maxCount = 0;
            string maxKaf = "";
            foreach (var kaf in kafs)
            {
                var count = from s in db.GetTable<Students>()
                            where s.SKAF == kaf
                            select s;
                if (maxCount < count.Count())
                {
                    maxCount = count.Count();
                    maxKaf = kaf;
                }
            }


            Console.WriteLine(maxKaf);
            
        }

        static void secondLINQ()
        {
            DataContext db = new DataContext(connectionString);

            var query = from s in db.GetTable<Students>()
                        where s.BDAY.Year == 1990
                        join t in db.GetTable<Teachers>()
                        on s.TID equals t.TID
                        where t.TFIO == "Rudakov Igor Vladimirovich"
                        select new { name = s.SFIO };

            foreach (var stud in query)
            {
                Console.WriteLine(stud.name);
            }
        }

        static void thirdLINQ()
        {
            DataContext db = new DataContext(connectionString);

            var query = from t in db.GetTable<Teachers>()
                        where t.TKAF == "L" 
                        where t.TMAXS == (
                           from s in db.GetTable<Students>()
                           where s.TID == t.TID
                           select s.STID
                        ).Count()
                        select new { name = t.TFIO };
            foreach (var t in query)
            {
                Console.WriteLine(t.name);
            }
        }

        static void firstADO()
        {
            string query = @"select top(1) first_part.SKAF, (select count(STID) from Students as second_part where first_part.SKAF = second_part.SKAF) as amount
                            from Students first_part where first_part.TID is null order by amount desc";

            SqlConnection connection = new SqlConnection(connectionString);

            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);

            int max = 0;
            string kaf_name;
            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                kaf_name = reader.GetValue(0).ToString();
                max = (int)reader.GetValue(1);

                Console.WriteLine(kaf_name + ", amount: " + max.ToString());
            }
        }

        static void secondADO()
        {
            string query = @"select s.SFIO from Students s join Teachers t on s.TID = t.TID
                            where year(s.BDAY) = 1990 and t.TFIO = 'Rudakov Igor Vladimirovich'";

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
                Console.WriteLine(reader.GetValue(0));
        }

        static void thirdADO()
        {
            string query = @"select t.TFIO from Teachers t where t.TKAF = 'L' and t.TMAXS =
                            (select count(STID) from Students s where s.TID = t.TID)";

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
                Console.WriteLine(reader.GetValue(0));
        }
    }
}
