﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public class GetTeachers
{
    private class Result
    {
        public SqlString _teacher_name;
        public SqlString _student_name;

        public Result(SqlString teacher_name, SqlString student_name)
        {
            _teacher_name = teacher_name;
            _student_name = student_name;
        }
    }

    [Microsoft.SqlServer.Server.SqlFunction(
        DataAccess = DataAccessKind.Read,
        FillRowMethodName = "GetTeacher_FillRow",
        TableDefinition = "TeacherName nvarchar(50), StudentName nvarchar(50)"
        )]
    public static IEnumerable GetTeacherCLR()
    {
        ArrayList result = new ArrayList();

        using (SqlConnection connection = new SqlConnection("context connection=true"))
        {
            connection.Open();
            using (SqlCommand cmd = new SqlCommand(
                "select first_part.SFIO, t.TFIO from Students first_part join Teachers t on first_part.TID = t.TID " +
                "where t.TMAXS < (select count(STID) from Students second_part where second_part.TID is null) and first_part.TID is null", connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(new Result(
                            reader.GetSqlString(0),
                            reader.GetSqlString(1)
                            ));
                    }
                }
            }
        }

        return result;
    }

    public static void GetHead_FillRow(object obj, out SqlString student_name, out SqlString teacher_name)
    {
        Result res = (Result)obj;
        student_name = res._student_name;
        teacher_name = res._teacher_name;
    }
}
