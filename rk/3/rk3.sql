DROP TABLE Teachers
DROP TABLE Students

CREATE TABLE Teachers(
    TID int not null IDENTITY(1, 1) primary key,
    TFIO varchar(30) not null,
    TKAF varchar(30) not null,
    TMAXS int not null
)

CREATE TABLE Students(
    STID int not null IDENTITY(1, 1) primary key,
    SFIO VARCHAR(30) not null,
    BDAY Date not null,
    SKAF VARCHAR(30) not null,
    TID int
)

INSERT INTO Teachers(TFIO, TKAF, TMAXS) VALUES
('Rudakov Igor Vladimirovich', 'IU7', 6),
('Stroganov Yury Vladimirovich', 'IU7', 5),
('Kurov Andrey Vladimirovich', 'IU7', 6),
('Skorikov Tatyana Petrovna', 'L', 1),
('Karpushin Igor Federovich', 'IU7', 4),
('Nikolaev Mark Grigorievich', 'IU7', 2),
('Maulin Nikolay Petrovich', 'IU7', 3),
('Korolev Fedor Markovich', 'IU7', 7),
('Lomovskoy Igor Vladimirovich', 'IU7', 1);


INSERT INTO Students(SFIO, BDAY, SKAF, TID) VALUES
('Ivanov Ivan Ivanovich', '1990-09-25', 'IU7', 1),
('Petrov Petr Petrovich', '1987-11-12', 'L', 3),
('Fedorov', '1987-05-05', 'IU7', 2),
('Karsakov', '1982-07-25', 'IU7', 2),
('Gulaya', '1991-01-05', 'IU7', 7),
('Ispolatov', '1994-05-14', 'IU7', null),
('Markov', '1989-08-15', 'L', null),
('Opushin', '1967-11-18', 'IU7', 2),
('Grigoriev', '1998-12-05', 'IU7', 4),
('Marabyan', '1976-02-01', 'IU7', 1),
('Karlov', '1956-03-08', 'IU7', 4);

select t.TFIO from Teachers t where t.TKAF = 'L' and t.TMAXS =
                            (select count(STID) from Students s where s.TID = t.TID)

SELECT * FROM Students
SELECT * FROM Teachers