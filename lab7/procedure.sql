DROP PROCEDURE [dbo].[sp_GetRegionAvgArea]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetRegionAvgArea]
    @Weather NVARCHAR(50) = '%',
    @Level INT,
    @AvgArea INT OUT
AS
BEGIN
    SELECT @AvgArea = AVG(area)
    FROM Regions
    WHERE weather = @Weather AND difficulty = @Level
END;
