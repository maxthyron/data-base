﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

public class UserDataContext : DataContext
{
    public UserDataContext(string connectionString) : base(connectionString) {}

    [Function(Name = "dbo.sp_GetRegionAvgArea")]
    [return: Parameter(DbType = "Int")]
    public int GetRegionAvgArea(
        [Parameter(Name = "Weather", DbType = "NVarChar(50)")] string weather,
        [Parameter(Name = "Level", DbType = "Int")] int level,
        [Parameter(Name = "AvgArea", DbType = "Float")] ref int avgarea)
    {
        IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodBase.GetCurrentMethod())), weather, level, avgarea);
        avgarea = ((int)(result.GetParameterValue(2)));
        return ((int)(result.ReturnValue));
    }
}


namespace demo_linq
{
    class Program
    {
        #region Data

        [Table(Name = "Adventurers")]
        class Adventurers
        {
            [Column(Name = "id", IsPrimaryKey = true)]
            public int id { get; set; }

            [Column(Name = "name")]
            public string name { get; set; }

            [Column(Name = "surname")]
            public string surname { get; set; }

            [Column(Name = "age")]
            public int age { get; set; }

            [Column(Name = "character")]
            public string character { get; set; }

            [Column(Name = "level")]
            public int level { get; set; }

            [Column(Name = "class")]
            public string aclass { get; set; }
        }

        [Table(Name = "Regions")]
        class Regions
        {
            [Column(Name = "id", IsPrimaryKey = true)]
            public int id { get; set; }

            [Column(Name = "name")]
            public string name { get; set; }

            [Column(Name = "difficulty")]
            public int difficulty { get; set; }

            [Column(Name = "weather")]
            public string weather { get; set; }

            [Column(Name = "area")]
            public int area { get; set; }
        }

        [Table(Name = "Keys")]
        class Keys
        {
            [Column(Name = "id", IsPrimaryKey = true)]
            public int id { get; set; }

            [Column(Name = "adventurerID")]
            public int aid { get; set; }

            [Column(Name = "regionID")]
            public int rid { get; set; }

            [Column(Name = "monsterID")]
            public int mid { get; set; }

            [Column(Name = "date")]
            public DateTime date { get; set; }
        }

        class Adventurer
        {
             public int AdventurerID { get; set; }
             public string Name { get; set; }
             public int Age { get; set; }
             public int RegionID { get; set; }
        }

        class Region
        {
            public string RegionName { get; set; }
            public int ID { get; set; }
        }

        // Specify the first data source.
        List<Adventurer> adventurers = new List<Adventurer>()
        {
           new Adventurer(){ AdventurerID = 001, Name="Alex", Age = 43, RegionID=001},
           new Adventurer(){ AdventurerID = 001, Name="Benedict", Age = 32, RegionID=002},
           new Adventurer(){ AdventurerID = 001, Name="Falmer", Age = 53, RegionID=003},
           new Adventurer(){ AdventurerID = 001, Name="Nod", Age = 12, RegionID=004},
           new Adventurer(){ AdventurerID = 001, Name="Maxwell", Age = 35, RegionID=005},
           new Adventurer(){ AdventurerID = 001, Name="Kale", Age = 25, RegionID=001},
           new Adventurer(){ AdventurerID = 001, Name="Lillia", Age = 87, RegionID=002},
           new Adventurer(){ AdventurerID = 001, Name="Brooks", Age = 65, RegionID=003},
           new Adventurer(){ AdventurerID = 001, Name="Pearl", Age = 32, RegionID=004},
           new Adventurer(){ AdventurerID = 001, Name="Loaf", Age = 21, RegionID=005}
        };

        // Specify the second data source.
        List<Region> regions = new List<Region>()
        {
            new Region{ RegionName="Hell",  ID=001},
            new Region{ RegionName="Wind Forest",  ID=002},
            new Region{ RegionName="Death Lake", ID=003},
            new Region{ RegionName="Dragon's Lair", ID=004},
            new Region{ RegionName="Frozen", ID=005},
            new Region{ RegionName="Bok Choy", ID=006},
            new Region{ RegionName="Peaches", ID=007},
            new Region{ RegionName="Melons", ID=008},
        };

        static void printMenu()
        {
            Console.WriteLine("1. First LINQ");
            Console.WriteLine("2. Second LINQ");
            Console.WriteLine("3. Third LINQ");
            Console.WriteLine("4. Fourth LINQ");
            Console.WriteLine("5. Fifth LINQ");
            Console.WriteLine("6. XML Child");
            Console.WriteLine("7. XML Rename");
            Console.WriteLine("8. ConnSql");
            Console.WriteLine("9. Insert Row");
            Console.WriteLine("10. Delete Row");
            Console.WriteLine("11. Update Row");
            Console.WriteLine("12. Procedure");
            Console.WriteLine("\n -- 0. Exit");
        }

        #endregion

        static void Main(string[] args)
        {
            Program app = new Program();
            bool flag = true;

            while (flag)
            {
                printMenu();
                Console.Write("\nEnter your choice > ");
                string choice = Console.ReadLine();
                Console.Clear();
                switch (choice)
                {
                    case "0":
                        flag = false;
                        break;
                    case "1":
                        app.first();
                        break;
                    case "2":
                        app.second();
                        break;
                    case "3":
                        app.third();
                        break;
                    case "4":
                        app.fourth();
                        break;
                    case "5":
                        app.fifth();
                        break;
                    case "6":
                        app.ChildXml();
                        break;
                    case "7":
                        app.RenameXml();
                        break;
                    case "8":
                        app.ConnSql();
                        break;
                    case "9":
                        app.InsertRow();
                        break;
                    case "10":
                        app.DeleteRow();
                        break;
                    case "11":
                        app.UpdateRow();
                        break;
                    case "12":
                        app.StroredProc();
                        break;
                }
                Console.WriteLine("Enter any key...");
                Console.Read();
                Console.Clear();
            }
        }

        void first()
        {
            var groupJoinQuery =
                from adventurer in adventurers
                orderby adventurer.RegionID
                join region in regions on adventurer.RegionID equals region.ID into adventurersGroup
                select new
                {
                    Adventurers = adventurer.Name,
                    Regions = from region in adventurersGroup
                               orderby region.RegionName
                               select region
                };

            int totalItems = 0;

            Console.WriteLine("GroupInnerJoin:");
            foreach (var adventurerGroup in groupJoinQuery)
            {
                Console.WriteLine(adventurerGroup.Adventurers);
                foreach (var regs in adventurerGroup.Regions)
                {
                    totalItems++;
                    Console.WriteLine("  {0,-10} {1}", regs.RegionName, regs.ID);
                }
            }
            Console.WriteLine("GroupInnerJoin: {0} items in {1} named groups", totalItems, groupJoinQuery.Count());
            Console.WriteLine(System.Environment.NewLine);
        }

        void second()
        {
            var nestedQueries = from a in adventurers
                                where a.Age > 18 && a.RegionID ==
                                    (from reg in regions
                                     where reg.RegionName == "Hell"
                                     select a.RegionID).FirstOrDefault()
                                select a;

            nestedQueries.ToList().ForEach(a => Console.WriteLine(a.Name));
        }

        void third()
        {
            var advreg = from a in adventurers
                                      join reg in regions
                                      on a.RegionID equals reg.ID
                                      select new
                                      {
                                          Name = a.Name,
                                          RegionName = reg.RegionName
                                      };

            advreg.ToList().ForEach(a => Console.WriteLine("{0} is in {1}", a.Name, a.RegionName));
        }

        void fourth()
        {
            var sortedadv = from a in adventurers
                                 orderby a.RegionID, a.Age
                                 select new
                                 {
                                     Name = a.Name,
                                     Age = a.Age,
                                     RegionID = a.RegionID
                                 };

            sortedadv.ToList().ForEach(a => Console.WriteLine("Adventurer Name: {0}, Age: {1}, RegionID: {2}", a.Name, a.Age, a.RegionID));
        }

        void fifth()
        {
            var adventurersWithRegions = from reg in regions
                                       join a in adventurers
                                       on reg.ID equals a.RegionID
                                       into ra
                                       from ra_grp in ra
                                       orderby reg.RegionName, ra_grp.Name
                                       select new
                                       {
                                           Name = ra_grp.Name,
                                           RegionName = reg.RegionName
                                       };


            foreach (var pair in adventurersWithRegions)
            {
                Console.WriteLine("{0} is in {1}", pair.Name, pair.RegionName);
            }
        }


        void ChildXml()
        {
            XElement root = XElement.Load("/Users/thyron/Documents/DataBase/lab5/task1/task1-explicit.xml");
            IEnumerable<XElement> address =
                from el in root.Elements("Monster")
                where (string)el.Element("name") == "Ahmed"
                select el;
            foreach (XElement el in address)
                Console.WriteLine(el);
        }

        void RenameXml()
        {
            XElement root = XElement.Load("/Users/thyron/Documents/DataBase/lab5/task1/task1-explicit.xml");

            foreach (XElement el in root.Elements())
                if ((string)el.Element("name") == "Ahmed")
                {
                    el.Element("name").Value = "Alesia";
                }

            root.Add(new XElement("Changes", new XElement("New_Name", "Alesia")));
            root.Save("new.xml");
        }

        void ConnSql()
        {
            string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";
            DataContext db = new DataContext(connectionString);

            Table<Adventurers> adventurer = db.GetTable<Adventurers>();
            Table<Regions> regs = db.GetTable<Regions>();
            Table<Keys> keys = db.GetTable<Keys>();

            var Query =
                from adv in adventurer
                where adv.id < 10
                select adv;

            Console.WriteLine("Monotable query:");
            foreach (var adv in Query)
            {
                Console.WriteLine("{0} \t{1} \t{2}", adv.id, adv.name, adv.character);
            }


            var Query2 =
                from reg in regs
                orderby reg.id
                join key in keys on reg.id equals key.rid into newGroup
                select new
                {
                    Reg =
                        from temp in newGroup
                        orderby temp.date
                        select temp

                };

            Console.WriteLine("Multitable query:");
            foreach (var temp in Query2)
            {
                foreach (var reg in temp.Reg)
                {
                    Console.WriteLine("{0, 10} {1, 10}", reg.aid, reg.date);
                }
            }
        }

        void InsertRow()
        {
            string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";
            DataContext db = new DataContext(connectionString);
            Table<Adventurers> advs = db.GetTable<Adventurers>();

            //Insert
            Console.WriteLine(advs.Count());
            Adventurers adv = new Adventurers
            {
                id = advs.Count() + 1,
                name = "July",
                surname = "Maxwell",
                age = 45,
                character = "Brave",
                level = 65,
                aclass = "Archer"
            };

            advs.InsertOnSubmit(adv);
            Console.WriteLine("Inserted Row");
            db.SubmitChanges();
        }

        void DeleteRow()
        {
            string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";
            DataContext db = new DataContext(connectionString);
            Table<Adventurers> advs = db.GetTable<Adventurers>();

            //Delete
            Console.WriteLine(advs.Count());

            //Adventurers adv = new Adventurers
            //{
            //    id = 101,
            //    name = "July",
            //    surname = "Maxwell",
            //    age = 45,
            //    character = "Brave",
            //    level = 65,
            //    aclass = "Archer"
            //};

            var adv = (from a in advs // $$$
                      where a.id == 101
                      select a).Single();

            //advs.Attach(adv);
            //advs.DeleteOnSubmit(adv); // БЕЗ Attach()
            advs.DeleteOnSubmit(adv);
            Console.WriteLine("Deleted Row");
            db.SubmitChanges();
        }

        void UpdateRow()
        {
            string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";
            DataContext db = new DataContext(connectionString);
            Table<Adventurers> advs= db.GetTable<Adventurers>();

            //Update

            var query = (from adv in advs
                         where adv.id == 100
                         select adv).FirstOrDefault();

            var querySingle = (from adv in advs
                               where adv.id == 99
                               select adv).Single(); // Single $$$


            query.name = "CHANGED"; // $$$
            query.surname = "UNIT";

            querySingle.name = "SINGLE"; // $$$
            querySingle.surname = "UNIT";

            Console.WriteLine("Updated Row");
            db.SubmitChanges();
        }

        void StroredProc()
        {
            string connectionString = @"Data Source=localhost; user id=sa; password=SqlConnect:3; database=ProjectDB";
            UserDataContext db = new UserDataContext(connectionString);

            int level = 5;
            int avgarea = 10;
            string weather = "Rainy";
            db.GetRegionAvgArea(weather, level, ref avgarea);
            Console.WriteLine("Средняя площадь региона с погодой {0} и сложностью {1}: {2}", weather, level, avgarea);
        }
    }
}
