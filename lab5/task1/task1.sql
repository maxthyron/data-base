-- RAW
SELECT name, surname, class
FROM Adventurers
ORDER BY name
FOR XML RAW, TYPE

-- AUTO
SELECT name, surname, class
FROM Adventurers
ORDER BY name
FOR XML AUTO, TYPE

-- PATH
SELECT name, surname, class
FROM Adventurers
ORDER BY name
FOR XML PATH('Adventurer'), TYPE

-- EXPLICIT
SELECT 1 AS Tag, NULL AS Parent, 
id AS [Monster!1!id],
name AS [Monster!1!name!ELEMENT], 
type AS [Monster!1!type!ELEMENT]
FROM Monsters
ORDER BY [Monster!1!id]
FOR XML EXPLICIT, TYPE, ROOT ('Monsters')

SELECT 1 AS Tag, NULL AS Parent, 
id AS [Adventurer!1!id],
name AS [Adventurer!1!name!ELEMENT], 
surname AS [Adventurer!1!surname!ELEMENT],
age AS [Adventurer!1!age!ELEMENT], 
character AS [Adventurer!1!character!ELEMENT], 
level AS [Adventurer!1!level!ELEMENT], 
class AS [Adventurer!1!class!ELEMENT]
FROM Adventurers
ORDER BY [Adventurer!1!id]
FOR XML EXPLICIT, TYPE, ROOT ('Adventurers')
