DECLARE @idoc INT;
DECLARE @doc XML;

SELECT @doc = c FROM OPENROWSET(BULK '/DB/lab5/task1/task1-explicit.xml', SINGLE_BLOB) AS TEMP(c)
EXEC sp_xml_preparedocument @idoc OUTPUT, @doc;

SELECT * FROM OPENXML (@idoc, N'/Monsters/Monster', 2)
WITH (id INT '@id', name VARCHAR(100), type VARCHAR(100));

EXEC sp_xml_removedocument @idoc;
