DECLARE @DocHandle int  
DECLARE @XmlDocument XML

SET @XmlDocument = (
            SELECT id AS "@id", name, type
            FROM Monsters
            ORDER BY id
            FOR XML PATH ('Monster'), TYPE, ROOT ('Monsters')
            );

EXEC sp_xml_preparedocument @DocHandle OUTPUT, @XmlDocument  

SELECT *  
FROM OPENXML (@DocHandle, '/Monsters/Monster', 2)  
      WITH(id INT '@id',
            name VARCHAR(100),
            type VARCHAR(100)) 

EXEC sp_xml_removedocument @DocHandle