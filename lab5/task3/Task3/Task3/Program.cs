﻿using System;
using System.Xml;
using System.Xml.Schema;

namespace ConsoleApp1
{
    class Program
    {
        private static bool flag;
        static void Main(string[] args)
        {
            flag = true;
            XmlSchemaCollection sc = new XmlSchemaCollection();
            sc.Add("", "/Users/thyron/Documents/DataBase/lab5/task3/task1-explicit.xsd");

            XmlTextReader tr = new XmlTextReader("/Users/thyron/Documents/DataBase/lab5/task1/task1-unknown.xml");
            //XmlTextReader tr = new XmlTextReader("/Users/thyron/Documents/DataBase/lab5/task1/task1-explicit.xml");

            XmlValidatingReader vr = new XmlValidatingReader(tr);
            vr.ValidationType = ValidationType.Schema;
            vr.Schemas.Add(sc);

            vr.ValidationEventHandler += new ValidationEventHandler(MyHandler);

            try
            {
                while (vr.Read())
                {
                    if (vr.NodeType == XmlNodeType.Element && vr.LocalName == "name")
                    {
                        var strName = vr.ReadElementString();
                        Console.Write("name: " + strName + " ");
                    }
                    else if (vr.NodeType == XmlNodeType.Element && vr.LocalName == "type")
                    {
                        var strType = vr.ReadElementString();
                        Console.WriteLine("type: " + strType);
                    }
                    else{

                    }
                }
            }
            catch (XmlException ex)
            {
                flag = false;
                Console.WriteLine("XmlException: " + ex.Message);
            }
            finally
            {
                vr.Close();
            }
            if (flag == true)
            {
                Console.WriteLine("Success!");
            }
            Console.ReadKey();
        }

        public static void MyHandler(object sender, ValidationEventArgs e)
        {
            flag = false;
            Console.WriteLine("Validation failed! Error message: " + e.Message);
        }
    }
}
