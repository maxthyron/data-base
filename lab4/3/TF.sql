CREATE ASSEMBLY SqlServerTVF
FROM
'/root/DataBase/lab4/3/TF.dll'
GO

CREATE FUNCTION NameToAscii ( @InputName NVARCHAR(4000) )
RETURNS TABLE 
(
   charpart NCHAR, 
   intpart INT
)
AS
EXTERNAL NAME
SqlServerTVF.[SqlServerTVF.UserDefinedFunctions].NameToAscii 
GO

SELECT * FROM dbo.NameToAscii ('Phillupins' )
GO
