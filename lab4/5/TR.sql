USE ProjectDB
GO

CREATE ASSEMBLY MyTriggers from '/root/DataBase/lab4/5/TR.dll';  
GO

CREATE TRIGGER DropTableTrigger  
ON DATABASE
FOR DROP_TABLE
AS EXTERNAL NAME MyTriggers.CLRTriggers.DropTableTrigger;  
GO

CREATE TABLE Table_temp(c1 int);  
GO

DROP TABLE Table_temp
GO

DROP TRIGGER DropTableTrigger ON DATABASE
DROP ASSEMBLY MyTriggers