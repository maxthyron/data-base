USE ProjectDB
GO

Create Assembly SqlServerUDF
FROM '/root/DataBase/lab4/def/def.dll'
GO

CREATE TYPE dbo.Region  
EXTERNAL NAME SqlServerUDF.[Region];
GO

CREATE TABLE dbo.Test
( 
  id INT IDENTITY(1,1) NOT NULL, 
  r Region NULL,
);
GO

-- Testing inserts
-- Correct values 
INSERT INTO dbo.Test(r) VALUES('6,100050'); 
INSERT INTO dbo.Test(r) VALUES('3,1235123'); 
INSERT INTO dbo.Test(r) VALUES('9,12345123');  
GO 
-- An incorrect value 
INSERT INTO dbo.Test(r) VALUES('5,21235412');
GO

-- Check the data - byte stream

SELECT id, r.ToString() AS Region 
FROM dbo.Test;

DECLARE @r1 dbo.Region
SET @r1 = CAST('7,51235' AS dbo.Region)
SELECT @r1.Level() AS 'Region Level'
GO

DROP TABLE dbo.Test
GO

DROP TYPE Region
GO

DROP ASSEMBLY SqlServerUDF
GO