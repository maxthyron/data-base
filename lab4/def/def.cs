using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text;
using System.IO;

[Serializable]
[Microsoft.SqlServer.Server.SqlUserDefinedType(Microsoft.SqlServer.Server.Format.UserDefined, IsByteOrdered = true, MaxByteSize = 8000)]
public struct Adventurer : INullable, IComparable, IBinarySerialize
{
    private bool is_Null;
    private string _Aname;
    private Int32 _Alevel;

    private byte[] N_Bytes;
    public bool IsNull
    {
        get
        {
            return (is_Null);
        }
    }

    public static Adventurer Null
    {
        get
        {
            Adventurer ad = new Adventurer();
            ad.is_Null = true;
            return ad;
        }
    }
    public SqlBinary Utf8Bytes  
    {  
        get  
        {  
            if (this.IsNull)  
                return SqlBinary.Null;  

            if (this.N_Bytes != null)  
                return this.N_Bytes;  

            if (this._Aname != null)  
            {  
                this.N_Bytes = System.Text.Encoding.UTF8.GetBytes(this._Aname);  
                return new SqlBinary(this.N_Bytes);  
            }  

            throw new NotSupportedException("cannot return bytes for empty instance");  
        }  
        set  
        {  
            if (value.IsNull)  
            {  
                this.N_Bytes = null;  
                this._Aname = null;  
            }  
            else  
            {  
                this.N_Bytes = value.Value;  
                this._Aname = null;  
            }  
        }  
    }  

    public override string ToString()
    {
        if (this.IsNull)
            return "NULL";
        else
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Aname);
            builder.Append(", ");   
            builder.Append(Alevel);
            return builder.ToString();
        }
    }
        public int CompareTo(object obj)  
        {  
            if (!(obj is Adventurer))  
                return -1;  

            Adventurer a = (Adventurer)obj;  

            if (this.is_Null && a.is_Null)  
                return 0;  

            if (this.is_Null || a.is_Null)  
                return -1;  

            if (this._Aname == a._Aname && this._Alevel == a._Alevel)  
                return 0;

            return 0;
        }

    #region IBinarySerialize Members  
    public void Write(BinaryWriter w)  
    {  
        if (w == null) throw new ArgumentException(string.Empty);  
        byte header = (byte)(this.IsNull ? 1 : 0);  

        w.Write(header);  
        if (header == 1)  
            return;  

        byte[] bytes = this.Utf8Bytes.Value;  

        w.Write(bytes.Length);  
        w.Write(bytes); 
    }

    public void Read(BinaryReader r)  
    {  
        if (r == null) throw new ArgumentException(string.Empty);  
        byte header = r.ReadByte();  

        if ((header & 1) > 0)  
        {  
            this.N_Bytes = null;  
            return;  
        }  

        int length = r.ReadInt32();  

        this.N_Bytes = r.ReadBytes(length);
    }  
    #endregion  
    [SqlMethod(OnNullCall = false)]
    public static Adventurer Parse(SqlString s)
    { 
        if (s.IsNull)
            return Null;
  
        Adventurer ad = new Adventurer();
        string[] nsl = s.Value.Split(",".ToCharArray());
        ad.Aname = Parse(nsl[0]).ToString();
        ad.Alevel = Int32.Parse(nsl[1]);

        return ad;
    }
  
    public string Aname
    {
        get
        {
            return this._Aname;
        }

        set
        {
            _Aname = value;
        }
    }

    public Int32 Alevel
    {
        get
        {
            return this._Alevel;
        }

        set
        {
            _Alevel = value;
        }
    }

    [SqlMethod(OnNullCall = false)]
    public SqlString Name()
    {
        return (SqlString)this._Aname;
    }
}