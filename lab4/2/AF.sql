USE ProjectDB
GO

CREATE ASSEMBLY SqlServerAggregate
FROM '/root/DataBase/lab4/2/AF.dll'
GO

CREATE AGGREGATE Concatenator( @instr nvarchar(400) )
RETURNS nvarchar(MAX)
EXTERNAL NAME SqlServerAggregate.Concatenator
GO 

SELECT dbo.Concatenator( name ) AS Adventurers
FROM Adventurers
WHERE class = 'Warrior'

DROP AGGREGATE Concatenator
DROP ASSEMBLY SqlServerAggregate