using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace temp
{
    public partial class Temp
    {
        [Microsoft.SqlServer.Server.SqlFunction]
        public static SqlInt32 GetRandomNumber()
        {
            Random rnd = new Random();
            return rnd.Next();
        }
                
    }
}