USE ProjectDB
GO

CREATE ASSEMBLY HandWrittenUDF
FROM '/root/DataBase/lab4/1/UDF.dll'
GO

CREATE FUNCTION GetRandomNumber ()
RETURNS INT
AS
EXTERNAL NAME HandWrittenUDF.[temp.Temp].GetRandomNumber
GO

SELECT dbo.GetRandomNumber() AS RandomNumber
GO

DROP FUNCTION GetRandomNumber
DROP ASSEMBLY HandWrittenUDF
