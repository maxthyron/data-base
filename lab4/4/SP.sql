USE ProjectDB
GO

Create Assembly SqlServerStoredProc
From
'/root/DataBase/lab4/4/SP.dll'
GO

Create Procedure GetConcatenatedNames ( @Role NVARCHAR(4000) )
As
External Name
SqlServerStoredProc.SqlServerStoresProcClass.GetConcatenatedNames
GO

Create Procedure GetNames ( @Role NVARCHAR(4000) )
As
External Name
SqlServerStoredProc.SqlServerStoresProcClass.GetNames
GO

Exec dbo.GetConcatenatedNames 'warrior'
GO

Exec dbo.GetNames 'warrior'
GO

drop PROCEDURE dbo.GetConcatenatedNames
GO

drop PROCEDURE dbo.GetNames
GO

drop assembly SqlServerStoredProc
GO
