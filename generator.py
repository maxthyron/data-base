import random


ROWS = 100


def get_from(file_name):
    with open(file_name) as file:
        array = [row.strip() for row in file]
    return array


def adventurers_csv(adventurers_names, surnames, characters, classes):
    f = open("csv/adventurers.csv", "w")
    f.write("id,name,surname,age,character,level,class\n")

    for i in range(1, ROWS + 1):
        name = random.choice(adventurers_names)
        surname = random.choice(surnames)
        age = random.randint(18, 100)
        character = random.choice(characters)
        level = random.randint(1, 100)
        theclass = random.choice(classes)
        string = "%d,%s,%s,%d,%s,%d,%s\n" % (i, name, surname, age, character, level, theclass)
        f.write(string)

    f.close()


def monsters_csv(monsters_names, loot, types):
    f = open("csv/monsters.csv", "w")
    f.write("id,name,level,uniqueLoot,type\n")

    for i in range(1, ROWS + 1):
        name = random.choice(monsters_names)
        level = random.randint(1, 100)
        theloot = random.choice(loot)
        thetype = random.choice(types)
        string = "%d,%s,%d,%s,%s\n" % (i, name, level, theloot, thetype)
        f.write(string)

    f.close()


def regions_csv(regions, weathers):
    f = open("csv/regions.csv", "w")
    f.write("id,name,difficulty,weather,area\n")

    for i in range(1, ROWS + 1):
        name = regions[i - 1]
        difficulty = random.randint(1, 10)
        weather = random.choice(weathers)
        area = random.randint(10000, 100000)
        string = "%d,%s,%d,%s,%d\n" % (i, name, difficulty, weather, area)
        f.write(string)

    f.close()


def keys_csv():
    f = open("csv/keys.csv", "w")
    f.write("id,adventurerID,regionID,monsterID,date\n")

    for i in range(1, ROWS + 1):
        adventurerID = random.randint(1, 100)
        regionID = random.randint(1, 100)
        monsterID = random.randint(1, 100)
        month = str(random.randint(1, 12))
        day = str(random.randint(1, 28))
        year = str(random.randint(1000, 2500))
        date = ".".join([month, day, year])
        string = "%d,%d,%d,%d,%s\n" % (i, adventurerID, regionID, monsterID, date)
        f.write(string)

    f.close()


def main():
    adventurers_names = get_from("txt/adventurersnames.txt")
    surnames = get_from("txt/adventurerssurnames.txt")
    monsters_names = get_from("txt/monstersnames.txt")
    characters = get_from("txt/characters.txt")
    classes = get_from("txt/classes.txt")
    loot = get_from("txt/loot.txt")
    regions = get_from("txt/regionsnames.txt")
    monsters_types = get_from("txt/types.txt")
    weathers = get_from("txt/weathers.txt")

    adventurers_csv(adventurers_names, surnames, characters, classes)
    regions_csv(regions, weathers)
    monsters_csv(monsters_names, loot, monsters_types)
    keys_csv()


if __name__ == '__main__':
    main()
